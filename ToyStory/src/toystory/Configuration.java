package toystory;

import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author glaucomunsberg
 */
public class Configuration {
    
    private JFrame jFrame;
    private JPanel canvas;
    private int coordinateX;
    private int coordinateY;
    private int radius;
    private int segment;
    private int repetitions;
    private static volatile Configuration instance = null;
    private final static Logger logger = Logger.getLogger(Configuration.class.getName());

    private Configuration(){
        coordinateX = coordinateY = 0;
        radius = segment= repetitions = 1;
        logger.info("Configuration is Loaded");
    }

    public static Configuration getInstance() {
        if (instance == null) {
            synchronized (Configuration.class) {
                if (instance == null) {
                    instance = new Configuration();
                }
            }
        }
        return instance;
    }

    public void setCanvas(JPanel canvas){
        this.canvas = canvas;
    }

    public void setFrame(JFrame jframe){
        this.jFrame = jframe;
    }

    public void setSegment(int segment){
        this.segment = segment;
    }

    public void setRadius(int radius){
        this.radius = radius;
    }
    
    public void setCoordinates(int x,int y){
        coordinateX = x;
        coordinateY = y;
    }

    public void setRepetitions(int repetitions){
        this.repetitions = repetitions;
    }

    public int getSegment(){
        return segment;
    }

    public int getRadius(){
        return radius;
    }

    public int getCoordinateX(){
        return coordinateX;
    }

    public int getCoordinateY(){
        return coordinateY;
    }

    public int getRepetitions(){
        return repetitions;
    }

    public JPanel getCanvas(){
        return canvas;
    }
    
    public JFrame getFrame(){
        return this.jFrame;
    }
    
    public void resetAll(){
        segment     = 0;
        radius      = 0;
        coordinateX = 0;
        coordinateY = 0;
        repetitions = 0;
        logger.info("All values are cleaned");
    }
}
