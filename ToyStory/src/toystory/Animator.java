/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package toystory;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Date;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author glaucomunsberg
 */
public class Animator extends Frame {

    private JPanel canvas;
    private static volatile Animator instance = null;
    private final static Logger logger = Logger.getLogger(Animator.class.getName());
    private static Configuration config;
    public static int w, h;

    private Animator() {
        config = Configuration.getInstance();
        logger.info("Animator is Loaded");
    }

    public static Animator getInstance() {
        if (instance == null) {
            synchronized (Animator.class) {
                if (instance == null) {
                    instance = new Animator();
                }
            }
        }
        return instance;
    }

    public void run() {
        canvas = config.getCanvas();
        int newWidth = (config.getCoordinateX() + config.getRadius()) + (config.getRepetitions() * 2 * config.getRadius()) + 50;
//        canvas.setSize(newWidth, canvas.getHeight());
//        config.getFrame().setSize(newWidth + 225, config.getFrame().getHeight());

        paint(canvas.getGraphics());
    }

    public void clean() {
        config.resetAll();
        canvas = new JPanel();
        config.getFrame().pack();
    }

    public void paint(Graphics g) {
        w = canvas.getWidth();
        h = canvas.getHeight();
        Area lastArea;
        int sizeOfPoint = 1;
        Graphics2D g2d = (Graphics2D) g;
        //g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);      
        Rectangle2D.Double rect1 = new Rectangle2D.Double(config.getCoordinateX() - config.getRadius(), config.getCoordinateY(), 12 * sizeOfPoint, 12 * sizeOfPoint);
        Rectangle2D.Double rectTemp = new Rectangle2D.Double(config.getCoordinateX() - config.getRadius(), config.getCoordinateY(), 12 * sizeOfPoint, 12 * sizeOfPoint);

        // Inverte o Y
        AffineTransform yUp = new AffineTransform();
        yUp.setToScale(1, -1);
        AffineTransform translate = new AffineTransform();
        translate.setToTranslation(40, h - 50);
        yUp.preConcatenate(translate);
        g2d.transform(yUp);
       // drawSimpleCoordinateSystem(300, 400, g2d);

        int steps = 100;
        double stepsDouble = steps;

        double[][] matPoints = new double[config.getSegment() * config.getRepetitions() + 1][2];
        int contPoints = 1;
        matPoints[0][0] = config.getCoordinateX() - config.getRadius();
        matPoints[0][1] = config.getCoordinateY();
        
        for (int i = 0; i < config.getRepetitions(); i++) {
            double angle = 180;

            boolean isRotate = false;
            while (angle > 0) {

                angle -= 180 / config.getSegment();
                double tX = config.getCoordinateX() + config.getRadius() * Math.cos(Math.toRadians(angle));
                double tY = config.getCoordinateY() + config.getRadius() * Math.sin(Math.toRadians(angle));
                
                matPoints[contPoints][0] = tX;
                matPoints[contPoints][1] = tY;
                contPoints++;
//                AffineTransform rotation = new AffineTransform();
//                if (isRotate) {
//                    rect1.setRect(tX, tY, 12 * sizeOfPoint, 12 * sizeOfPoint);
//                    rotation.setToRotation(-Math.PI / 2, rect1.getCenterX(), rect1.getCenterY());
//                    isRotate = false; 
//                } else {
//                    rect1.setRect(tX, tY, 15 * sizeOfPoint, 15 * sizeOfPoint);
//                    rotation.setToRotation(-Math.PI / 4, rect1.getCenterX(), rect1.getCenterY());
//                    isRotate = true;
//                }
//                                                               
//                //g2d.fill(rotation.createTransformedShape(r1));

            }
            config.setCoordinates(config.getCoordinateX() + 2 * config.getRadius(), config.getCoordinateY());
        }
        for (int i = 0; i < matPoints.length; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(matPoints[i][j] + " ");
            }
            System.out.println("");
        }

        boolean isRotate = false;

        for (int i = 0; i < config.getSegment() * config.getRepetitions(); i++) {

            rect1 = new Rectangle2D.Double(matPoints[i][0], matPoints[i][1], 12, 12);
            double xmid = matPoints[i][0] + (12) / 2.0;
                        
            double ymid = matPoints[i][1] + (12) / 2.0;

            AffineTransform initialTransform = new AffineTransform();
            initialTransform.setToTranslation(matPoints[i][0], matPoints[i][1]);
            //initialTransform.setToRotation(-Math.PI/2,xmid,ymid);

            double[] initialMatrix = new double[6];
            initialTransform.getMatrix(initialMatrix);

            AffineTransform finalTransform = new AffineTransform();
            finalTransform.setToTranslation(matPoints[i + 1][0], matPoints[i + 1][1]);
            finalTransform.concatenate(scalingWRTXY(xmid, ymid, 1.6, 1.6));
            finalTransform.rotate(-Math.PI / 4, xmid, ymid);
            //finalTransform.scale(1.6, 1.6);

            double[] finalMatrix = new double[6];
            finalTransform.getMatrix(finalMatrix);

//            AffineTransform initialTransform = new AffineTransform();
//            initialTransform.setToTranslation(matPoints[i][0],matPoints[i][1]);
//            
//            AffineTransform finalTransform = new AffineTransform();
//            finalTransform.setToTranslation(matPoints[i+1][0],matPoints[i+1][1]);
//            double[] finalMatrix = new double[6];
//            double[] initialMatrix = new double[6];
//            
//            initialTransform.getMatrix(initialMatrix);
//            finalTransform.getMatrix(finalMatrix);
//                                    
            Shape s;
            AffineTransform intermediateTransform;

            for (int j = 0; j <= steps; j++) {
                sustain(10);
                intermediateTransform = new AffineTransform(convexCombination(initialMatrix, finalMatrix, j / stepsDouble));
                s = intermediateTransform.createTransformedShape(rect1);
                // clearWindow(g2d); 
                g2d.fill(s);
            }
            //rect1 = new Rectangle2D.Double(matPoints[i+1][0],matPoints[i+1][1], 12 , 12);
        }
    }

    public static void drawSimpleCoordinateSystem(int xmax, int ymax, Graphics2D g2d) {
        int xOffset = 0;
        int yOffset = 0;
        int step = 20;
        String s;
        //aktuellen Font merken
        Font fo = g2d.getFont();
        //einen kleinen Font einstellen
        int fontSize = 13;
        Font fontCoordSys = new Font("serif", Font.PLAIN, fontSize);
        /*
         Leider werden die Buchstaben durch die auf das Graphics2D-Objekt
         angewendete Transformation auf dem Kopf gezeichnt. Daher: Erzeugung
         eines Fonts, bei dem die Buchstaben auf dem Kopf stehen.
         */
        //Auf den Kopf stellen durch Spiegeln.
        AffineTransform flip = new AffineTransform();
        flip.setToScale(1, -1);
        //Wieder auf die Grundlinie zurueck verschieben.
        AffineTransform lift = new AffineTransform();
        lift.setToTranslation(0, fontSize);
        flip.preConcatenate(lift);

        //Font mit auf den Kopf gestellten Buchstaben erzeugen.
        Font fontUpsideDown = fontCoordSys.deriveFont(flip);

        g2d.setFont(fontUpsideDown);

        //x-Achse
        g2d.drawLine(xOffset, yOffset, xmax, yOffset);
        //Markierungen und Beschriftung der x-Achse
        for (int i = xOffset + step; i <= xmax; i = i + step) {
            g2d.drawLine(i, yOffset - 2, i, yOffset + 2);
            g2d.drawString(String.valueOf(i), i - 7, yOffset - 30);
        }

        //y-Achse
        g2d.drawLine(xOffset, yOffset, xOffset, ymax);

        //Markierungen und Beschriftung der y-Achse
        s = "  "; //zum Einruecken der Zahlen < 100
        for (int i = yOffset + step; i <= ymax; i = i + step) {
            g2d.drawLine(xOffset - 2, i, xOffset + 2, i);
            if (i > 99) {
                s = "";
            }
            g2d.drawString(s + String.valueOf(i), xOffset - 25, i - 20);
        }

        //Font zurueck setzen
        g2d.setFont(fo);
    }

    public static void clearWindow(Graphics2D g) {
        g.setPaint(Color.white);
        g.fill(new Rectangle(0, 0, w, h));
        g.setPaint(Color.black);
    }

    public static void sustain(long t) {
        long finish = (new Date()).getTime() + t;
        while ((new Date()).getTime() < finish) {
        }
    }

    public static double[] convexCombination(double[] a, double[] b, double alpha) {
        double[] result = new double[a.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = (1 - alpha) * a[i] + alpha * b[i];
        }

        return (result);
    }

    public static AffineTransform scalingWRTXY(double x, double y, double xs, double ys) {
        AffineTransform at = new AffineTransform();

        at.translate(x, y);
        at.scale(xs, ys);
        at.translate(-x, -y);

        return (at);
    }    
}
