/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.*;
import java.awt.geom.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.TimerTask;

import figurinhas.*;
/**
 *
 * @author regiszanandrea
 */
public class Figurinhas extends TimerTask {
    public static int width = 1024, height = 1024;
    public TriangulatedImage img[];
    public BufferedImageDrawer bid;
    public BufferedImage bg;
    public BufferedImage mix;
    public int segments, repetitions, radius,steps, count = 0;
    public double x, y;
    private double alpha,deltaAlpha;
    public ArrayList<Point2D> pontos;
    public ArrayList<String> files;
    public ArchiveReader archive;
    private TriangulatedImage t1;
    private double stepsDouble;
  //The second triangulated image.
  private TriangulatedImage t2;

    /**
     * @param args the command line arguments
     */

    Figurinhas(BufferedImageDrawer bid, BufferedImage backGround, int segments, int repetitions, int radius, double x, double y) {
        this.steps = 100;
        this.stepsDouble = steps;
        this.deltaAlpha = 1.0/steps;
        this.alpha = 0;
        this.bid = bid;
        this.bg = backGround;
        this.segments = segments;
        this.repetitions = repetitions;
        this.radius = radius;
        this.x = x;
        this.y = y;
        this.bid.g2dbi.drawImage(bg,0,0,null);
        this.pontos = new ArrayList<>();
        files = new ArrayList<>();
        archive = new ArchiveReader("../points.txt");
        
//         AffineTransform yUp = new AffineTransform();
//        yUp.setToScale(1, -1);
//        AffineTransform translate = new AffineTransform();
//        translate.setToTranslation(40, height - 50);
//        yUp.preConcatenate(translate);
//        bid.g2dbi.transform(yUp);
//        
        AffineTransform yUp = new AffineTransform();
        yUp.setToScale(1, -1);
        AffineTransform translate = new AffineTransform();
        translate.setToTranslation(40, this.height - 50);
        yUp.preConcatenate(translate);
        bid.g2dbi.transform(yUp);
        
        String path = "imagens/";
        File f = new File(path);
        String[] filelist = f.list();        
        for (int i = 0; i < filelist.length; i++){
            if(!filelist[i].equals(".DS_Store")){
                this.files.add(filelist[i]);
            }
        }       
        
        /*** Calcula os pontos ***/
        
        pontos.add(new Point2D.Double(this.x-this.radius, this.y));
        for (int i = 0; i < this.repetitions; i++) {
            double angle = 180;

            boolean isRotate = false;
            while (angle > 0) {

                angle -= 180 / this.segments;
                double tX = this.x + this.radius * Math.cos(Math.toRadians(angle));
                double tY = this.y + this.radius * Math.sin(Math.toRadians(angle));
                
                pontos.add(new Point2D.Double(tX, tY));
                
            }
            this.x = x + 2*this.radius;                        
        }
        
        /*** Printa os pontos ***/
//        for (int i = 0; i < pontos.size(); i++) {
//           System.out.println("X: "+pontos.get(i).getX()+"Y: "+pontos.get(i).getY());
//        }
        archive = new ArchiveReader("points.txt");
        int points[][][] = new int[9][9][2];

        for(int a=0; a < 8;a++){          
          for(int b=0;b < 8;b++){
            points[a][b][0] = (int)archive.getPointsByPicture(a,b).getX();
            points[a][b][1] = (int)archive.getPointsByPicture(a,b).getY();
          }
        }
        int[][] triangles =  {{0,1,8}, {0,7,8}, {7,8,9}, {7,6,9}, 
        {6,9,5}, {5,9,12}, {5,12,4}, {4,12,3},
        {3,2,10}, {3,12,10}, {2,11,1}, {1,11,8}, {8,10,11}, {8,9,10},
        {10,11,12},{10,9,12}};
    
    img = new TriangulatedImage[16];
    Image loadimage;
    int im;
        for (int i = 0; i < 9; i++) {
          im = i+1;
          img[i] = new TriangulatedImage();
          img[i].bi = new BufferedImage(width, height, 1);
          Graphics2D g2dt1 = img[i].bi.createGraphics();
          
          loadimage = new javax.swing.ImageIcon("imagens/imagem" + im + ".jpg").getImage();
          g2dt1.drawImage(loadimage, 0, 0, null);
          
          
          //pontos das imagens
          //usado como pontos das bordas (0,0), (0,y), (x, y), (x,0)
            img[i].tPoints = new Point2D[13];
            img[i].tPoints[0] = new Point2D.Double(0, 0);
            img[i].tPoints[1] = new Point2D.Double(0, height / 2);
            img[i].tPoints[2] = new Point2D.Double(0, height);
            img[i].tPoints[3] = new Point2D.Double(width / 2, height);
            img[i].tPoints[4] = new Point2D.Double(width, height);
            img[i].tPoints[5] = new Point2D.Double(width, height / 2);
            img[i].tPoints[6] = new Point2D.Double(width, 0);
            img[i].tPoints[7] = new Point2D.Double(width / 2, 0);
            img[i].tPoints[8] = new Point2D.Double(points[i][0][0], points[i][0][1]);
            img[i].tPoints[9] = new Point2D.Double(points[i][1][0], points[i][1][1]);
            img[i].tPoints[10] = new Point2D.Double(points[i][4][0],points[i][4][1]);
            img[i].tPoints[11] = new Point2D.Double(points[i][2][0], points[i][2][1]);
            img[i].tPoints[12] = new Point2D.Double(points[i][3][0], points[i][3][1]);
            
            
            //pontos dos triangulos
            img[i].triangles = new int[16][3];
            for (int j = 0; j < 16; j++) {
                img[i].triangles[j][0] = triangles[j][0];
                img[i].triangles[j][1] = triangles[j][1];
                img[i].triangles[j][2] = triangles[j][2];
            }
            
        }
    }


    public static void clearWindow(Graphics2D g) {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();
        g.setPaint(Color.black);
        g.fill(new Rectangle(0, 0, d.width, d.height));
        g.setPaint(Color.black);
    }
    public static void drawSimpleCoordinateSystem(int xmax, int ymax, Graphics2D g2d) {
        int xOffset = 0;
        int yOffset = 0;
        int step = 20;
        String s;
        //aktuellen Font merken
        Font fo = g2d.getFont();
        //einen kleinen Font einstellen
        int fontSize = 13;
        Font fontCoordSys = new Font("serif", Font.PLAIN, fontSize);
        /*
         Leider werden die Buchstaben durch die auf das Graphics2D-Objekt
         angewendete Transformation auf dem Kopf gezeichnt. Daher: Erzeugung
         eines Fonts, bei dem die Buchstaben auf dem Kopf stehen.
         */
        //Auf den Kopf stellen durch Spiegeln.
        AffineTransform flip = new AffineTransform();
        flip.setToScale(1, -1);
        //Wieder auf die Grundlinie zurueck verschieben.
        AffineTransform lift = new AffineTransform();
        lift.setToTranslation(0, fontSize);
        flip.preConcatenate(lift);

        //Font mit auf den Kopf gestellten Buchstaben erzeugen.
        Font fontUpsideDown = fontCoordSys.deriveFont(flip);

        g2d.setFont(fontUpsideDown);

        //x-Achse
        g2d.drawLine(xOffset, yOffset, xmax, yOffset);
        //Markierungen und Beschriftung der x-Achse
        for (int i = xOffset + step; i <= xmax; i = i + step) {
            g2d.drawLine(i, yOffset - 2, i, yOffset + 2);
            g2d.drawString(String.valueOf(i), i - 7, yOffset - 30);
        }

        //y-Achse
        g2d.drawLine(xOffset, yOffset, xOffset, ymax);

        //Markierungen und Beschriftung der y-Achse
        s = "  "; //zum Einruecken der Zahlen < 100
        for (int i = yOffset + step; i <= ymax; i = i + step) {
            g2d.drawLine(xOffset - 2, i, xOffset + 2, i);
            if (i > 99) {
                s = "";
            }
            g2d.drawString(s + String.valueOf(i), xOffset - 25, i - 20);
        }

        //Font zurueck setzen
        g2d.setFont(fo);
    }
    public void sustain(long t) {
        long finish = (new Date()).getTime() + t;
        while ((new Date()).getTime() < finish) {
        }
    }
    public static double[] convexCombination(double[] a, double[] b, double alpha) {
        double[] result = new double[a.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = (1 - alpha) * a[i] + alpha * b[i];
        }

        return (result);
    }

    public static void main(String[] args) {
        // Args = x y raio segmentos repetições

        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        BufferedImageDrawer bid = new BufferedImageDrawer(bi, width, height);
        bid.setTitle("Figurinhas");

        BufferedImage backGround = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2dBackGround = backGround.createGraphics();

        g2dBackGround.setPaint(Color.black);
        g2dBackGround.fill(new Rectangle(0, 0, width, height));
        Figurinhas fig;
        if (args.length == 5) {
            fig = new Figurinhas(bid, backGround, Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[2]), Double.parseDouble(args[0]), Double.parseDouble(args[1]));
        } else {
            // Configuração defaut
            fig = new Figurinhas(bid, backGround, 4, 2, 50, 300, 300);
        }
        
        Timer t = new Timer();
        t.scheduleAtFixedRate(fig,0,1000);
    }

    @Override
    public void run() {
        
        BufferedImage bgImagem = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB); 
        Graphics2D g2dbgImagem = bgImagem.createGraphics(); 

        g2dbgImagem.setPaint(Color.black); 
        g2dbgImagem.fill(new Rectangle(0,0,width,height)); 
              
        /*** Morphing ***/
//        if (alpha>=0 && alpha<=1){                                                
//            mix = img[count].mixWith(img[count+1],alpha);
//            for (int j = 0; j <= steps; j++) {
//                
//                int dx = (int)((1 - alpha) * pontos.get(count).getX()) + (int)((alpha) * pontos.get(count+1).getX()); 
//                int dy = (int)((1 - alpha) * pontos.get(count).getY()) + (int)((alpha) * pontos.get(count+1).getY());                                 
//                                            
//                bid.g2dbi.drawImage(mix,dx,dy,null);
//                bid.repaint();                           
//            }            
//        }else{
//            alpha = 0;
//            count++;
//        }
        if(img.length == 8){
            sustain(80000);
        }
        if (alpha>=0 && alpha<=1){   
            mix = img[count].mixWith(img[count+1],alpha);
            bid.g2dbi.drawImage(mix,200,200,null);
            bid.repaint();
        }else{
            alpha = 0;
            count++;
        }
        //Increment alpha.
        alpha = alpha+0.1;
      
    }

}
