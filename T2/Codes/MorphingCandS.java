import java.awt.*;
import java.awt.geom.*;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.image.BufferedImage;

/**
* A simple example for transforming one triangulated image into another one.
* For the animation, the doube buffering technique is applied in the same way 
* as in the clock example in the class DoubeBufferingClockExample.
*
* @author Frank Klawonn
* Last change 31.05.2005
*
* @see TriangulatedImage
* @see BufferImageDrawer
* @see DoubeBufferingClockExample
*/
public class MorphingCandS extends TimerTask
{

  //The window in which the transformation is shown.
  private BufferedImageDrawer buffid;

  //The two images to be transformed into each other will be scaled to 
  //this size.
  private int width;
  private int height;

  //The number of steps (frames) for the transformation.
  private int steps;

  //The first triangulated image.
  private TriangulatedImage t1;

  //The second triangulated image.
  private TriangulatedImage t2;

  //This is used for generating/storing the intermediate images.
  private BufferedImage mix;

  //A variable which is increased stepwise from 0 to 1. It is needed
  //for the computation of the convex combinations.
  private double alpha;

  //The change of alpha in each step: deltAlpha = 1.0/steps
  private double deltaAlpha;


  /**
   * Constructor
   *
   * @param bid    The window in which the transformation is shown.
   */
  MorphingCandS(BufferedImageDrawer bid)
  {
    buffid = bid;

    width = 230;
    height = 300;

    steps = 100;

    deltaAlpha = 1.0/steps;

    alpha = 0;

    //This object is used for loading the two images.
    Image loadedImage;

    //Generating the first triangulated image:
    t1 = new TriangulatedImage();

    //Define the size.
    t1.bi = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);

    //Generate the Graphics2D object.
    Graphics2D g2dt1 = t1.bi.createGraphics();

    //Load the image and draw it on the corresponding BufferedImage.
    loadedImage = new javax.swing.ImageIcon("Balotelli.jpg").getImage();
    g2dt1.drawImage(loadedImage,0,0,null);

    //Definition of the points for the triangulation.
    t1.tPoints = new Point2D[23];
    //Pontos diferentes
    t1.tPoints[0] = new Point2D.Double(110,83);
    t1.tPoints[1] = new Point2D.Double(134,84);
    t1.tPoints[2] = new Point2D.Double(80,86);
    t1.tPoints[3] = new Point2D.Double(162,91);
    t1.tPoints[4] = new Point2D.Double(103,125);
    t1.tPoints[5] = new Point2D.Double(138,128);
    t1.tPoints[6] = new Point2D.Double(82,146);
    t1.tPoints[7] = new Point2D.Double(159,142);
    //Pontos Iguais
    //a
    t1.tPoints[8] = new Point2D.Double(0,0);
    t1.tPoints[9] = new Point2D.Double(60,0);
    t1.tPoints[10] = new Point2D.Double(130,0);
    t1.tPoints[11] = new Point2D.Double(230,0);
    t1.tPoints[12] = new Point2D.Double(0,60);
    t1.tPoints[13] = new Point2D.Double(0,75);
    t1.tPoints[14] = new Point2D.Double(0,140);
    t1.tPoints[15] = new Point2D.Double(0,300);
    t1.tPoints[16] = new Point2D.Double(60,300);
    t1.tPoints[17] = new Point2D.Double(160,300);
    t1.tPoints[18] = new Point2D.Double(230,300);
    t1.tPoints[19] = new Point2D.Double(230,40);
    t1.tPoints[20] = new Point2D.Double(230,60);
    t1.tPoints[21] = new Point2D.Double(230,160);
    //p
    t1.tPoints[22] = new Point2D.Double(120,210);

    // t1.tPoints[0] = new Point2D.Double(0,0);
    // t1.tPoints[1] = new Point2D.Double(0,110);
    // t1.tPoints[2] = new Point2D.Double(0,150);
    // t1.tPoints[3] = new Point2D.Double(50,150);
    // t1.tPoints[4] = new Point2D.Double(100,150);
    // t1.tPoints[5] = new Point2D.Double(100,110);
    // t1.tPoints[6] = new Point2D.Double(100,0);
    // t1.tPoints[7] = new Point2D.Double(50,0);
    // t1.tPoints[8] = new Point2D.Double(50,110);


    //Definition of the triangles.
    t1.triangles = new int[31][3];

    t1.triangles[0][0] = 8;
    t1.triangles[0][1] = 12;
    t1.triangles[0][2] = 0;
    
    t1.triangles[1][0] = 12;
    t1.triangles[1][1] = 13;
    t1.triangles[1][2] = 0;
    
    t1.triangles[2][0] = 13;
    t1.triangles[2][1] = 0;
    t1.triangles[2][2] = 2;
    
    t1.triangles[3][0] = 13;
    t1.triangles[3][1] = 14;
    t1.triangles[3][2] = 2;
    
    t1.triangles[4][0] = 14;
    t1.triangles[4][1] = 2;
    t1.triangles[4][2] = 6;
    
    t1.triangles[5][0] = 14;
    t1.triangles[5][1] = 15;
    t1.triangles[5][2] = 6;
    
    t1.triangles[6][0] = 15;
    t1.triangles[6][1] = 16;
    t1.triangles[6][2] = 6;
    
    t1.triangles[7][0] = 16;
    t1.triangles[7][1] = 6;
    t1.triangles[7][2] = 22;
    
    t1.triangles[8][0] = 16;
    t1.triangles[8][1] = 22;
    t1.triangles[8][2] = 17;
    
    t1.triangles[9][0] = 17;
    t1.triangles[9][1] = 22;
    t1.triangles[9][2] = 7;
    
    t1.triangles[10][0] = 17;
    t1.triangles[10][1] = 18;
    t1.triangles[10][2] = 7;
    
    t1.triangles[11][0] = 18;
    t1.triangles[11][1] = 21;
    t1.triangles[11][2] = 7;
    
    t1.triangles[12][0] = 21;
    t1.triangles[12][1] = 7;
    t1.triangles[12][2] = 3;
    
    t1.triangles[13][0] = 3;
    t1.triangles[13][1] = 21;
    t1.triangles[13][2] = 20;
    
    t1.triangles[14][0] = 20;
    t1.triangles[14][1] = 3;
    t1.triangles[14][2] = 1;
    
    t1.triangles[15][0] = 20;
    t1.triangles[15][1] = 19;
    t1.triangles[15][2] = 1;
    
    t1.triangles[16][0] = 1;
    t1.triangles[16][1] = 19;
    t1.triangles[16][2] = 11;
    
    t1.triangles[17][0] = 1;
    t1.triangles[17][1] = 11;
    t1.triangles[17][2] = 10;
    
    t1.triangles[18][0] = 10;
    t1.triangles[18][1] = 1;
    t1.triangles[18][2] = 0;
    
    t1.triangles[19][0] = 0;
    t1.triangles[19][1] = 10;
    t1.triangles[19][2] = 1;
    
    t1.triangles[20][0] = 0;
    t1.triangles[20][1] = 10;
    t1.triangles[20][2] = 9;

    t1.triangles[21][0] = 9;
    t1.triangles[21][1] = 0;
    t1.triangles[21][2] = 8;
    
    t1.triangles[22][0] = 0;
    t1.triangles[22][1] = 2;
    t1.triangles[22][2] = 4;
    
    t1.triangles[23][0] = 0;
    t1.triangles[23][1] = 4;
    t1.triangles[23][2] = 5;
        
    t1.triangles[24][0] = 0;
    t1.triangles[24][1] = 5;
    t1.triangles[24][2] = 1;
    
    t1.triangles[25][0] = 1;
    t1.triangles[25][1] = 5;
    t1.triangles[25][2] = 3;
        
    t1.triangles[26][0] = 3;
    t1.triangles[26][1] = 5;
    t1.triangles[26][2] = 7;
    
    t1.triangles[27][0] = 7;
    t1.triangles[27][1] = 5;
    t1.triangles[27][2] = 22;
        
    t1.triangles[28][0] = 22;
    t1.triangles[28][1] = 4;
    t1.triangles[28][2] = 5;
    
    t1.triangles[29][0] = 22;
    t1.triangles[29][1] = 4;
    t1.triangles[29][2] = 6;

    t1.triangles[30][0] = 4;
    t1.triangles[30][1] = 6;
    t1.triangles[30][2] = 2;


    //The same for the second image.
    t2 = new TriangulatedImage();

    t2.bi = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
    Graphics2D g2dt2 = t2.bi.createGraphics();

    loadedImage = new javax.swing.ImageIcon("Cebolla.jpg").getImage();

    g2dt2.drawImage(loadedImage,0,0,null);

    t2.tPoints = new Point2D[23];
    t2.tPoints[0] = new Point2D.Double(110,86);
    t2.tPoints[1] = new Point2D.Double(130,86);
    t2.tPoints[2] = new Point2D.Double(76,102);
    t2.tPoints[3] = new Point2D.Double(164,103);
    t2.tPoints[4] = new Point2D.Double(108,126);
    t2.tPoints[5] = new Point2D.Double(138,125);
    t2.tPoints[6] = new Point2D.Double(82,155);
    t2.tPoints[7] = new Point2D.Double(163,155);
    t2.tPoints[8] = new Point2D.Double(100,100);
    //Pontos Iguais
    //a
    t2.tPoints[8] = new Point2D.Double(0,0);
    t2.tPoints[9] = new Point2D.Double(60,0);
    t2.tPoints[10] = new Point2D.Double(130,0);
    t2.tPoints[11] = new Point2D.Double(230,0);
    t2.tPoints[12] = new Point2D.Double(0,60);
    t2.tPoints[13] = new Point2D.Double(0,75);
    t2.tPoints[14] = new Point2D.Double(0,140);
    t2.tPoints[15] = new Point2D.Double(0,300);
    t2.tPoints[16] = new Point2D.Double(60,300);
    t2.tPoints[17] = new Point2D.Double(160,300);
    t2.tPoints[18] = new Point2D.Double(230,300);
    t2.tPoints[19] = new Point2D.Double(230,40);
    t2.tPoints[20] = new Point2D.Double(230,60);
    t2.tPoints[21] = new Point2D.Double(230,160);
    //p
    t2.tPoints[22] = new Point2D.Double(120,210);

    // t2.tPoints[0] = new Point2D.Double(0,0);
    // t2.tPoints[1] = new Point2D.Double(0,40);
    // t2.tPoints[2] = new Point2D.Double(0,150);
    // t2.tPoints[3] = new Point2D.Double(55,150);
    // t2.tPoints[4] = new Point2D.Double(100,150);
    // t2.tPoints[5] = new Point2D.Double(100,40);
    // t2.tPoints[6] = new Point2D.Double(100,0);
    // t2.tPoints[7] = new Point2D.Double(55,0);
    // t2.tPoints[8] = new Point2D.Double(55,40);


    //The indexing for the triangles must be the same as in the
    //the first image.
    t2.triangles = t1.triangles;
    System.out.println("Triangulos: ");
    for (int i=0; i<31; i++){
      System.out.println("["+t1.triangles[i][0]+" "+t1.triangles[i][1]+" "+t1.triangles[i][2]+"]");
    }
    System.out.println("final");
  }


  //This method is called in regular intervals. This method computes
  //the updated image/frame and calls the repaint method to draw the
  //updated image on the window.
  public void run()
  {

    //Since this method is called arbitrarily often, interpolation must only
    //be carred out while alpha is between 0 and 1.
    if (alpha>=0 && alpha<=1)
    {
      //Generate the interpolated image.
      mix = t1.mixWith(t2,alpha);

      //Draw the interpolated image on the BufferedImage.
      buffid.g2dbi.drawImage(mix,230,300,null);

      //Call the method for updating the window.
      buffid.repaint();
    }

    //Increment alpha.
    alpha = alpha+deltaAlpha;

  }


  public static void main(String[] argv)
  {

    //Width of the window.
    int width = 230;
    //Height of the window.
    int height = 500;

    //Specifies (in milliseconds) when the frame should be updated.
    int delay = 50;

    //The BufferedImage to be drawn in the window.
    BufferedImage bi = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);


    //The window in which everything is drawn.
    BufferedImageDrawer bid = new BufferedImageDrawer(bi,width,height);
    bid.setTitle("Transforming shape and colour");

    //The TimerTask in which the repeated computations for drawing take place.
    MorphingCandS mcs = new MorphingCandS(bid);


    Timer t = new Timer();
    t.scheduleAtFixedRate(mcs,0,delay);

  }

}

