import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.awt.geom.*;

public class ArchiveReader{

	Picture pictures[];

	public ArchiveReader(String file){
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
		    
		    String line;
		    int position_line=0;
		    int position_picture =0;
		    pictures = new Picture[8];

		    while ((line = br.readLine()) != null) {
		       
		       if(line.equals("")){

		       		System.out.println("Quebra");
		       		position_line = 0;

		       }else{

		       		if(position_line == 0){
		       			System.out.println("File: "+line);
		       			pictures[position_picture] = new Picture(line);
		       			position_picture++;
		       		}else{
		       			String[] parts = line.split(" ");
		       			pictures[position_picture-1].setPoints(position_line-1,Integer.parseInt(parts[0]),Integer.parseInt(parts[1]));
		       			

		       			System.out.println(line);
		       		}
		       		position_line++;
		       		
		       }

		    }

		}catch(Exception e){

			e.printStackTrace();

		}
	}

	public Point2D getPointsByPicture(int picture,int position){
		return pictures[picture].getPoints(position);
	}

	public String getNameByPicture(int picture){
		return pictures[picture].getName();
	}


	static public void main(String[] argv){
		ArchiveReader file = new ArchiveReader("../points.txt");
		
		System.out.println("Informações da 1ª figura:");
		System.out.println(file.getNameByPicture(0));
		System.out.println(file.getPointsByPicture(0,0));

		System.out.println("Informações da 8ª figura:");
		System.out.println(file.getNameByPicture(7));
		System.out.println(file.getPointsByPicture(7,7));
	}
}

class Picture{

	String name;
	Point2D points[];

	public Picture(String name){
		this.name = name;
		points = new Point2D[8];
	}

	public void setPoints(int position,int point1,int point2){
		points[position] = new Point2D.Double(point1,point2);
	}

	public Point2D getPoints(int position){
		return points[position];
	}


	public String getName(){
		return name;
	}

	public String getPointString(int position){
		return "["+points[position]+" "+points[position]+"]";
	}

}