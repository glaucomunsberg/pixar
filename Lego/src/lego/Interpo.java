/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lego;

import com.sun.j3d.loaders.IncorrectFormatException;
import com.sun.j3d.loaders.ParsingErrorException;
import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.universe.SimpleUniverse;
import java.awt.Color;
import static java.awt.Component.TOP_ALIGNMENT;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import javax.media.j3d.Alpha;
import javax.media.j3d.Appearance;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Material;
import javax.media.j3d.PositionInterpolator;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.JFrame;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
import static lego.Lego.setToMyDefaultAppearance;

/**
 *
 * @author glaucoroberto
 */
public class Interpo extends JFrame{
    public Canvas3D myCanvas3D;
    public Interpo(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        //Default settings for the viewer parameters.
        myCanvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());

        SimpleUniverse simpUniv = new SimpleUniverse(myCanvas3D);
        simpUniv.getViewingPlatform().setNominalViewingTransform();
        createSceneGraph(simpUniv);
        addStaticLight(simpUniv);


        //The following three lines enable navigation through the scene using the mouse.
        OrbitBehavior ob = new OrbitBehavior(myCanvas3D);
        ob.setSchedulingBounds(new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE));
        simpUniv.getViewingPlatform().setViewPlatformBehavior(ob);


        //Show the canvas/window.
        setTitle("LEGO® World™");
        setSize(1024,720);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        getContentPane().add("Center", myCanvas3D);
        setVisible(true);
    }
    
    
    public static void main(String[] args)
    {
        Interpo legoWorld = new Interpo();
    }
    
    public void createSceneGraph(SimpleUniverse su)
    {
        Background bg = new Background(new Color3f(Color.lightGray));
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE);
        bg.setApplicationBounds(bounds);
        BranchGroup theScene = new BranchGroup();
        
        Appearance lightBlueApp = new Appearance();
        setToMyDefaultAppearance(lightBlueApp,new Color3f(0.0f,0.1f,0.3f));
        Appearance lightGrayApp = new Appearance();
        setToMyDefaultAppearance(lightGrayApp,new Color3f(Color.DARK_GRAY));
        Appearance lightWhiteApp = new Appearance();
        Material m = new Material();
        m.setDiffuseColor(0.1f,0.9f,0,9f);
        m.setSpecularColor(1.0f,0.0f,0.0f);
        m.setAmbientColor(0.9f,0.9f,0.9f);
        m.setShininess(25.0f);
        lightWhiteApp.setMaterial(m);
        TransparencyAttributes ta = new TransparencyAttributes(TransparencyAttributes.NICEST,0.7f);
        lightWhiteApp.setTransparencyAttributes(ta);
        setToMyDefaultAppearance(lightWhiteApp,new Color3f(Color.white));
        
        ObjectFile f = new ObjectFile(ObjectFile.LOAD_ALL);
        f.setFlags(ObjectFile.RESIZE);
        
        Scene helicopter =null;
        try
        {
          helicopter = f.load(getClass().getResource("helicopter.obj").getFile() );
        }
        catch (FileNotFoundException | IncorrectFormatException | ParsingErrorException e)
        {
          System.out.println("Erro ao carregar:" + e);
        }
        Hashtable namedObjects = helicopter.getNamedObjects();
        Shape3D partOfTheObject = (Shape3D) namedObjects.get("blue_brick");
        partOfTheObject = (Shape3D) namedObjects.get("l3460");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3823");
        partOfTheObject.setAppearance(lightWhiteApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3822");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3821");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3462");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3481");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3039");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3004");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3032");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3040b");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3022");
        partOfTheObject.setAppearance(lightWhiteApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3460_0");
        partOfTheObject.setAppearance(lightBlueApp);
        partOfTheObject = (Shape3D) namedObjects.get("l3023_6");
        partOfTheObject.setAppearance(lightBlueApp);
        
        Transform3D tfRotateZ = new Transform3D();
        tfRotateZ.rotZ(Math.PI/3);
        TransformGroup tgObjectHelicop = new TransformGroup(tfRotateZ);
        Box rotor = new Box(0.4f,0.1f,0.01f,lightBlueApp);
        tgObjectHelicop.addChild(rotor);
        tgObjectHelicop.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        int timeStartWait = 1000;       //The helicopter should start its flight when the rotor
        int timeStartRotor = 1000;      //The rotor blade should start to turn after 2 seconds.
        int noStartRotations = 2;       //First, two slow rotations are carried out.
        int timeSlowRotation = 1500;    //A slow rotation takes 1.5 seconds.
        int timeOneWayFlight = 4000;    //The helicopter rises within 2 seconds.
        int timeAcc = 300;              //The acceleration and breaking phase should last 0.3 seconds.
        int timeHovering = 1000;        //It shall hover for one second.
        int timeFlightStart = timeStartRotor+timeSlowRotation*noStartRotations+timeStartWait;
        Transform3D som = new Transform3D();
        som.setScale(0.2f);
        
        Alpha helicopterAlpha = new Alpha(-1,Alpha.INCREASING_ENABLE,
                                      0,0,timeOneWayFlight+2000,timeAcc,
                                      timeHovering,timeOneWayFlight,timeAcc,0);
        
        PositionInterpolator posIPHelicopter = new PositionInterpolator(helicopterAlpha,
                                                   tgObjectHelicop);
        posIPHelicopter.setSchedulingBounds(bounds);
        posIPHelicopter.setStartPosition(TOP_ALIGNMENT);
        posIPHelicopter.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        posIPHelicopter.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        tgObjectHelicop.addChild(posIPHelicopter);
        Transform3D tfHelicopter = new Transform3D();
        TransformGroup tgHelicopter = new TransformGroup(tfHelicopter);
        tgHelicopter.addChild(tgObjectHelicop);
        theScene.addChild(tgHelicopter);
        theScene.addChild(bg);
        theScene.compile();
        //Add the scene to the universe.
        su.addBranchGraph(theScene);
    }
    
    public static void setToMyDefaultAppearance(Appearance app, Color3f col)
    {
        app.setMaterial(new Material(col,col,col,col,150.0f));
    }
    public void addStaticLight(SimpleUniverse su)
    {

        BranchGroup bgLight = new BranchGroup();

        BoundingSphere bounds = new BoundingSphere(new Point3d(2.0,2.0,9.0), 100.0);
        Color3f lightColour1 = new Color3f(1.0f,1.0f,1.0f);
        Vector3f lightDir1  = new Vector3f(-1.0f,0.0f,-0.5f);
        DirectionalLight light1 = new DirectionalLight(lightColour1, lightDir1);
        light1.setInfluencingBounds(bounds);

        bgLight.addChild(light1);
        
//        DirectionalLight lightSpin = new DirectionalLight(lightColour1, lightDir1);
//        lightSpin.setInfluencingBounds(bounds);
//        
//        TransformGroup tfmLight = new TransformGroup();
//        tfmLight.addChild(lightSpin);
//        Alpha alphaLight = new Alpha(-1,4000);
//        RotationInterpolator rot = new RotationInterpolator(alphaLight,tfmLight,
//                                                        new Transform3D(),
//                                                         0.0f,(float) Math.PI*2);
//        rot.setSchedulingBounds(bounds);
//        tfmLight.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
//        tfmLight.addChild(rot);
//        bgLight.addChild(tfmLight);
        
        su.addBranchGraph(bgLight);
    }
}
