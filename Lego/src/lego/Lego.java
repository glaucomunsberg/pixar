package lego;

import javax.vecmath.*;
import com.sun.j3d.utils.universe.*;
import javax.media.j3d.*;
import com.sun.j3d.utils.behaviors.vp.*;
import javax.swing.JFrame;
import com.sun.j3d.loaders.*;
import com.sun.j3d.loaders.objectfile.*;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Sphere;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Enumeration;

public class Lego extends JFrame
{

    public Canvas3D canvas3D;
    public Play play;
    
    int timeStartWait = 500;                   //The LegoMan and the helicopter should start its flight when the rotor
    int noStartRotations = 2;                   //First, two slow rotations are carried out.
    int timeSlowRotation = 1500;                //A slow rotation takes 1.5 seconds.
    int timeWayFlight = 4000;                //The helicopter rises within 4 seconds.
    int timeAcc = 300;                          //The acceleration and breaking phase should last 0.3 seconds.
    int timeHovering = 500;                    //It shall hover for one second.
    int timeFlightStart = timeSlowRotation*noStartRotations+timeStartWait;
    float radius = 0.1f;
    
    
    
    public Lego()
    {

        /**
         * Cria o canvas que servirá de universo para 
         *  a animação
         */
        canvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());

        SimpleUniverse simpUniv = new SimpleUniverse(canvas3D);
        simpUniv.getViewingPlatform().setNominalViewingTransform();
        createSceneGraph(simpUniv);
        addStaticLight(simpUniv);


        /**
         * Posiciona a camera em cima para uma melho
         */
        OrbitBehavior ob = new OrbitBehavior(canvas3D);
        ob.setSchedulingBounds(new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE));
        simpUniv.getViewingPlatform().setViewPlatformBehavior(ob);
     
        /**
         * Gera a janela e ocupa toda a tela
         */
        setTitle("LEGO® World™");
        setSize(1024,720);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setExtendedState(JFrame.MAXIMIZED_BOTH);
        getContentPane().add("Center", canvas3D);
        setVisible(true);
        
        
        /**
         * Executa a música
         */
        play = new Play("/home/glaucoroberto/Projetos/pixar/Lego/src/lego/EverythingIsAwesome.wav");
        //play.run();
    }




    public static void main(String[] args)
    {
        Lego legoWorld = new Lego();
    }

    /**
     * Metodo que insere todos os elementos da cena
     *  e também cria a animação
     * @param su 
     */
    public void createSceneGraph(SimpleUniverse su)
    {
        
        /**
         *  Cria as aparências que serão na animação
         */
        Appearance lightBlueApp = new Appearance();
        setToMyDefaultAppearance(lightBlueApp,new Color3f(0.0f,0.1f,0.3f));
        
        Appearance lightGrayApp = new Appearance();
        setToMyDefaultAppearance(lightGrayApp,new Color3f(Color.DARK_GRAY));
        
        Appearance lightWhiteApp = new Appearance();
        Material m = new Material();
        m.setDiffuseColor(0.1f,0.9f,0,9f);
        m.setSpecularColor(1.0f,0.0f,0.0f);
        m.setAmbientColor(0.9f,0.9f,0.9f);
        m.setShininess(25.0f);
        lightWhiteApp.setMaterial(m);
        TransparencyAttributes ta = new TransparencyAttributes(TransparencyAttributes.NICEST,0.7f);
        lightWhiteApp.setTransparencyAttributes(ta);
        setToMyDefaultAppearance(lightWhiteApp,new Color3f(Color.white));
        
        Appearance lightGreenApp = new Appearance();
        setToMyDefaultAppearance(lightGreenApp,new Color3f(0.2f,0.7f,0.2f));
        
        Appearance lightGrayLessApp = new Appearance();
        setToMyDefaultAppearance(lightGrayLessApp,new Color3f(Color.LIGHT_GRAY));
        
        Appearance lightRedApp = new Appearance();
        setToMyDefaultAppearance(lightRedApp,new Color3f(Color.RED));
        
        Appearance lightYellowApp = new Appearance();
        setToMyDefaultAppearance(lightYellowApp,new Color3f(Color.YELLOW));

        /**
         * auxis usado em pathInterpolator
         */
        AxisAngle4f axis = new AxisAngle4f(1.0f, 0.0f, 0.0f, 0.0f);
        
        
        /**
         * Cria o backgroud da cena
         *  no caso é pintado de cinza para evitar
         *  que se confunda com o banco de alguns objetos
         */
        Background bg = new Background(new Color3f(Color.lightGray));
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0),Double.MAX_VALUE);
        bg.setApplicationBounds(bounds);
        
        
        // ------------- Criação do Piso -------------
        
        ObjectFile f = new ObjectFile(ObjectFile.LOAD_ALL);
        f.setFlags(ObjectFile.RESIZE);
        Scene brick = null;

        try
        {
            brick = f.load(getClass().getResource("LegoBrick.obj").getFile() );
        }
        catch (FileNotFoundException | IncorrectFormatException | ParsingErrorException e)
        {
            System.out.println("Erro ao carregar o arquivo:" + e);
        }
 
        Hashtable namedObjects = brick.getNamedObjects();
        Shape3D partObj = (Shape3D) namedObjects.get("blue_brick");

        /**
         * Aqui é criado uma malha no chão de tijolos de lego
         */
        BranchGroup theScene = new BranchGroup();
        Shape3D brickGray;
        for(int a=-5;a < 5;a++){//-10 a 5
            brickGray = (Shape3D) partObj.cloneTree();
            brickGray.setAppearance(lightGrayApp);
            Transform3D tfObjectBricks = new Transform3D();
            tfObjectBricks.setScale(0.1f);
            Transform3D translationBricks = new Transform3D();
            float xPosition = (float)0.84*a;
            translationBricks.setTranslation(new Vector3f(xPosition,0.0f,0.0f));
            tfObjectBricks.mul(translationBricks);
            TransformGroup tgObject = new TransformGroup(tfObjectBricks);
            tgObject.addChild(brickGray);
            theScene.addChild(tgObject);
            
            for(int b=-10;b < 5;b++){ //-10 a 10
                brickGray = (Shape3D) partObj.cloneTree();
                brickGray.setAppearance(lightGrayApp);
                Transform3D tfObjectBricks2 = new Transform3D();
                tfObjectBricks2.setScale(0.1f);
                Transform3D translationBricks2 = new Transform3D();
                float zPostion = (float)0.45*b;
                translationBricks2.setTranslation(new Vector3f(xPosition,0.0f,zPostion));
                tfObjectBricks2.mul(translationBricks2);
                TransformGroup tgObjectBricks2 = new TransformGroup(tfObjectBricks2);
                tgObjectBricks2.addChild(brickGray);
                theScene.addChild(tgObjectBricks2);
            
            }
        }
        
        
        // ------------- Cria-se Helicoptero -------------
        
        Scene helicopter =null;
        try
        {
          helicopter = f.load(getClass().getResource("helicopter.obj").getFile() );
        }
        catch (FileNotFoundException | IncorrectFormatException | ParsingErrorException e)
        {
          System.out.println("Erro ao carregar:" + e);
        }

        /**
         * Pinta o helicóptero com as
         *  as cores e a frente fica transparente
         */
        namedObjects = helicopter.getNamedObjects();
        
        partObj = (Shape3D) namedObjects.get("l3460");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3823");
        partObj.setAppearance(lightWhiteApp);
        partObj = (Shape3D) namedObjects.get("l3822");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3821");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3462");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3481");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3039");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3004");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3032");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3040b");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3022");
        partObj.setAppearance(lightWhiteApp);
        partObj = (Shape3D) namedObjects.get("l3460_0");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("l3023_6");
        partObj.setAppearance(lightBlueApp);
        
        
        /**
         * Aplicação das transformações no Helicoptero
         *      translation -0.5,-2,0.59
         *      scale       0.1
         *      rotation Z  PI/2
         *      rotation X  -PI/2
         */
        Transform3D tfObjectHelicop = new Transform3D();
        tfObjectHelicop.setTranslation(new Vector3f(2f,-2f,0.9f));
        
        Transform3D tfScaleHelicop = new Transform3D();
        tfScaleHelicop.setScale(0.1f);
        tfScaleHelicop.mul(tfObjectHelicop);
        
        Transform3D tfRotateZ = new Transform3D();
        tfRotateZ.rotZ(Math.PI/2);
        tfRotateZ.mul(tfScaleHelicop);
        
        Transform3D tfRotateXHelicop = new Transform3D();
        tfRotateXHelicop.rotX(-Math.PI/2);
        tfRotateXHelicop.mul(tfRotateZ);
        
        TransformGroup tgObjectHelicop = new TransformGroup(tfRotateXHelicop);
        tgObjectHelicop.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tgObjectHelicop.addChild(helicopter.getSceneGroup());
        
        
        /**
         * Animation
         *  Criando a animação do Helicoptero
         */
        
        Transform3D helicopterMovimentation = new Transform3D();
        helicopterMovimentation.setTranslation(new Vector3f(0f,2f,2f));
       
        Alpha alphaHelicopter = new Alpha(-1,Alpha.INCREASING_ENABLE+Alpha.DECREASING_ENABLE,timeFlightStart,0,timeWayFlight,0,timeHovering,0,0,0);
        Transform3D axisOfRotHelicopter = new Transform3D();
        
        axisOfRotHelicopter.set(axis);
        Point3f[] squareHelicopter = createSquareHelicopter(20);
        float[] animKnotsHelicopter = createUniformAnimKnots(squareHelicopter.length);
        PositionPathInterpolator positionPathInterHelicopter = new PositionPathInterpolator(alphaHelicopter,tgObjectHelicop,axisOfRotHelicopter,animKnotsHelicopter,squareHelicopter);
        positionPathInterHelicopter.setSchedulingBounds(bounds);
        
        tgObjectHelicop.addChild(positionPathInterHelicopter);
        
        
        
        TransformGroup tgObjectHelicopterAnimation = new TransformGroup(tfRotateXHelicop);
        tgObjectHelicopterAnimation.addChild(tgObjectHelicop);
        
        theScene.addChild(tgObjectHelicopterAnimation);
        
        /**
         * Colição do Helicoptero
         *  crição do espaço que é considerado colição
         */
        tgObjectHelicop.setCollisionBounds(new BoundingBox(new Point3d(0.1,0.15,0.1),new Point3d(0.1,0.15,0.1)));
        tgObjectHelicop.setCollidable(true);
       
        
        // ------------- Criação do legoMan -------------
        
        Scene legoMan =null;
        Scene legoManDead =null;
        try
        {
          legoMan = f.load(getClass().getResource("LEGO.obj").getFile() );
          legoManDead = f.load(getClass().getResource("LEGO.obj").getFile() );
        }
        catch (FileNotFoundException | IncorrectFormatException | ParsingErrorException e)
        {
          System.out.println("Erro ao carregar:" + e);
        }
        
        
        /**
         * Aplicação de transformações
         *      scale       0.3
         *      translation -0,14,0.04,-0.4
         */
        Transform3D tfObjectLegoMan = new Transform3D();
        tfObjectLegoMan.setScale(0.03f);
        
        Transform3D tfTransLegoMan = new Transform3D();
        tfTransLegoMan.setTranslation(new Vector3f(-0.14f,0.04f,-0.4f));
        tfTransLegoMan.mul(tfObjectLegoMan);
        
        TransformGroup tgObjectLegoMan = new TransformGroup(tfTransLegoMan);
        tgObjectLegoMan.addChild(legoMan.getSceneGroup());
        tgObjectLegoMan.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        TransformGroup tgObjectLegoManDead = new TransformGroup(tfTransLegoMan);
        tgObjectLegoManDead.addChild(legoManDead.getSceneGroup());
        tgObjectLegoManDead.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        /**
         * Pintando o Obj
         *  para que fique colorido
         */
        namedObjects = legoMan.getNamedObjects();
        partObj = (Shape3D) namedObjects.get("mesh12");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh11");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh10");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh9");
        partObj.setAppearance(lightRedApp);
        partObj = (Shape3D) namedObjects.get("mesh8");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("mesh7");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("mesh6");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh5");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh4");
        partObj.setAppearance(lightRedApp);
        partObj = (Shape3D) namedObjects.get("mesh3");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("mesh2");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("mesh1");
        partObj.setAppearance(lightBlueApp);
        
        /**
         * Colisão
         *  criando a posibilidade de colição do objeto
         */
        tgObjectLegoMan.setCollisionBounds(new BoundingBox(new Point3d(0.5,0.15,0.5), new Point3d(0.1,0.15,0.1)));
        tgObjectLegoMan.setCollidable(true);
        
        /**
         * Animação
         *  criando a animação do LegoMan
         */
        Transform3D LegoManMovimentation = new Transform3D();
        LegoManMovimentation.setTranslation(new Vector3f(0f,2f,0f));
        
        Alpha legoManAlpha = new Alpha(-1,Alpha.INCREASING_ENABLE+Alpha.DECREASING_ENABLE,timeFlightStart,0,timeWayFlight-1000,timeAcc-100,timeHovering,0,0,0);
        
        PositionInterpolator posIPLegoMan = new PositionInterpolator(legoManAlpha,tgObjectLegoMan,LegoManMovimentation,0f,12f);
        posIPLegoMan.setSchedulingBounds(bounds);
        posIPLegoMan.setStartPosition(TOP_ALIGNMENT);
        posIPLegoMan.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        posIPLegoMan.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        tgObjectLegoMan.addChild(posIPLegoMan);
        
        TransformGroup tgLegoMan = new TransformGroup(tfTransLegoMan);
        tgLegoMan.addChild(tgObjectLegoMan);
        
        theScene.addChild(tgLegoMan);
        
        // ------------- Criação do airplane -------------

        Scene airplane =null;
        try
        {
          airplane = f.load(getClass().getResource("Ultralight-Airplane.obj").getFile() );
        }
        catch (FileNotFoundException | IncorrectFormatException | ParsingErrorException e)
        {
          System.out.println("Erro ao carregar o avião:" + e);
        }
        
        /**
         * Transformação no airplane
         *      scale       0.1f
         *      translation 0,0.06,-0.1
         *      
         */
        Transform3D tfObjectAirPlane = new Transform3D();
        tfObjectAirPlane.setScale(0.1f);
        
        Transform3D tfTransAirplane = new Transform3D();
        tfTransAirplane.setTranslation(new Vector3f(0f,0.06f,-0.1f));
        tfTransAirplane.mul(tfObjectAirPlane);
        
        TransformGroup tgObjectAirplane = new TransformGroup(tfTransAirplane);
        tgObjectAirplane.addChild(airplane.getSceneGroup());
        tgObjectAirplane.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        namedObjects = airplane.getNamedObjects();
        
        /**
         * Pinta inteiro de cinza
         */
        Enumeration enumer = namedObjects.keys();
        String name;
        while (enumer.hasMoreElements())
        {
            name = (String) enumer.nextElement();
            partObj = (Shape3D) namedObjects.get(name);
            partObj.setAppearance(lightGrayApp);
        }
        
        /**
         * Pinta o airplane
         *  algumas parte com as cores que se quer
         */
        partObj = (Shape3D) namedObjects.get("mesh8");
        partObj.setAppearance(lightGrayLessApp);
        partObj = (Shape3D) namedObjects.get("mesh18");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh19");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh21");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh32");
        partObj.setAppearance(lightGrayLessApp);
        partObj = (Shape3D) namedObjects.get("mesh35");
        partObj.setAppearance(lightGrayLessApp);
        partObj = (Shape3D) namedObjects.get("mesh39");
        partObj.setAppearance(lightWhiteApp);
        partObj = (Shape3D) namedObjects.get("mesh42");
        partObj.setAppearance(lightRedApp);
        partObj = (Shape3D) namedObjects.get("mesh43");
        partObj.setAppearance(lightRedApp);
        partObj = (Shape3D) namedObjects.get("mesh44");
        partObj.setAppearance(lightRedApp);
        partObj = (Shape3D) namedObjects.get("mesh45");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh47");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh49");
        partObj.setAppearance(lightBlueApp);
        partObj = (Shape3D) namedObjects.get("mesh48");
        partObj.setAppearance(lightYellowApp);
        partObj = (Shape3D) namedObjects.get("mesh50");
        partObj.setAppearance(lightYellowApp);
        
        
        /**
         * Transformação da posição da luz
         */
        Alpha alphaAirplane = new Alpha(-1, Alpha.INCREASING_ENABLE+Alpha.DECREASING_ENABLE,timeFlightStart,0,timeWayFlight+10000,0,timeHovering,0,0,0);
        Transform3D axisOfRotPosAirplane = new Transform3D();
        
        
        axisOfRotPosAirplane.set(axis);
        
        Point3f[] squareAirplane = createSquareAirPlane(20);
        float[] animKnotsAirPlane = createUniformAnimKnots(squareAirplane.length);
        PositionPathInterpolator positionPathInterAirplane = new PositionPathInterpolator(alphaAirplane, tgObjectAirplane, axisOfRotPosAirplane, animKnotsAirPlane, squareAirplane);
        positionPathInterAirplane.setSchedulingBounds(bounds);
        
        tgObjectAirplane.addChild(positionPathInterAirplane);
        
        TransformGroup tgObjectAirplaneAnimation = new TransformGroup(tfTransAirplane);
        tgObjectAirplaneAnimation.addChild(tgObjectAirplane);
        
        
        theScene.addChild(tgObjectAirplaneAnimation);
        
        
        // ------------- Criação do Casa Lego -------------
        
        Scene building =null;
        try
        {
          building = f.load(getClass().getResource("TV.obj").getFile() );
        }
        catch (Exception e)
        {
          System.out.println("Erro ao carregar:" + e);
        }
        
        /**
         * Transformação da Casa
         *  scale       0.5
         *  translation -0.69,0.11,-0.1
         */
        Transform3D tfObjectBuiling = new Transform3D();
        tfObjectBuiling.setScale(0.4f);
        
        Transform3D tfTransBuiling = new Transform3D();
        tfTransBuiling.setTranslation(new Vector3f(-0.3f,0.41f,-0.27f));
        tfTransBuiling.mul(tfObjectBuiling);
        
        TransformGroup tgObjectBuilding = new TransformGroup(tfTransBuiling);
        tgObjectBuilding.addChild(building.getSceneGroup());
        
        
        namedObjects = building.getNamedObjects();
        enumer = namedObjects.keys();
        while (enumer.hasMoreElements())
        {
            name = (String) enumer.nextElement();
            partObj = (Shape3D) namedObjects.get(name);
            partObj.setAppearance(lightGrayApp);
        }
        
        theScene.addChild(tgObjectBuilding);
        
        
        /**
         * Colision
         */
       
        Switch colourSwitch = new Switch();
        colourSwitch.setCapability(Switch.ALLOW_SWITCH_WRITE);
        
        //colourSwitch.addChild(tgObjectLegoMan);
        //colourSwitch.addChild(tgObjectLegoManDead);
        colourSwitch.setCollisionBounds(new BoundingSphere(new Point3d(0.0,0.0,0.0),radius));
        colourSwitch.setCollidable(true);
        colourSwitch.setWhichChild(0);
        
        Transform3D tfSphere = new Transform3D();
        tfSphere.setTranslation(new Vector3f(0.7f,0.0f,0.0f));
        TransformGroup tgSphere = new TransformGroup(tfSphere);
        tgSphere.addChild(colourSwitch);
        
        theScene.addChild(tgSphere);
        
        /**
         * Background e Compilação
         *  adiciona o background e compila
         */
        
        theScene.addChild(bg);
        theScene.compile();
        
        //Add the scene to the universe.
        su.addBranchGraph(theScene);
    }


 
    public static void setToMyDefaultAppearance(Appearance app, Color3f col)
    {
        app.setMaterial(new Material(col,col,col,col,150.0f));
    }

    public void addStaticLight(SimpleUniverse su)
    {

        /**
         * BrachGroup das Luzes
         */
        BranchGroup     bgLight = new BranchGroup();
        BoundingSphere  bounds  = new BoundingSphere(new Point3d(2.0,2.0,9.0), 100.0);
        
        Color3f     lightColour    = new Color3f(1.0f,1.0f,1.0f);
        Vector3f    lightDirection  = new Vector3f(-1.0f,0.0f,-0.5f);
        
        /**
         * Luz direcional
         */
        DirectionalLight lightDirectional = new DirectionalLight(lightColour, lightDirection);
        lightDirectional.setInfluencingBounds(bounds);
        bgLight.addChild(lightDirectional);
        
        /**
         * Luz se movimenta em circulos
         */
        DirectionalLight lightSpin = new DirectionalLight(lightColour, lightDirection);
        lightSpin.setInfluencingBounds(bounds);
        
        
        Appearance lightBlueApp = new Appearance();
        setToMyDefaultAppearance(lightBlueApp,new Color3f(0.0f,0.1f,0.3f));
        Sphere bol = new Sphere(0.01f,lightBlueApp);
        bol.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        Transform3D positionLight = new Transform3D();
        positionLight.setTranslation(new Vector3f(0f,0.06f,-0.1f));
        TransformGroup tgLight = new TransformGroup(positionLight);
        tgLight.addChild(lightSpin);
        tgLight.addChild(bol);
        tgLight.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        /**
         * Movimento da luz
         */
        Alpha alphaAirplane = new Alpha(-1, Alpha.INCREASING_ENABLE+Alpha.DECREASING_ENABLE,timeFlightStart,0,timeWayFlight+10000,0,timeHovering,0,0,0);
        Transform3D axisOfRotPos = new Transform3D();
        
        AxisAngle4f axis = new AxisAngle4f(1.0f, 0.0f, 0.0f, 0.0f);
        axisOfRotPos.set(axis);
        
        Point3f[] squareAirplane = createSquareAirPlane(5);
        float[] animKnotsAirPlane = createUniformAnimKnots(squareAirplane.length);
        PositionPathInterpolator positionPathInterAirplane = new PositionPathInterpolator(alphaAirplane, tgLight, axisOfRotPos, animKnotsAirPlane, squareAirplane);
        positionPathInterAirplane.setSchedulingBounds(bounds);
        
        tgLight.addChild(positionPathInterAirplane);
        
        
        
        
        TransformGroup tfSomething = new TransformGroup();
        tfSomething.addChild(tgLight);
        
        
        
        bgLight.addChild(tfSomething);
        su.addBranchGraph(bgLight);
    }
    
    
    private Point3f[] createSquareAirPlane(int size){
        Point3f[] square = new Point3f[size];
        float progressA = (float) 0.3;
        float progressB = (float) 0.1;
        for(int a=0; a < size; a++){
            float positionA = progressA*a;
            float positionB = progressB*a;
            square[a] = new Point3f(0f,positionA,positionB); 
            System.out.println("A:"+Float.toString(positionA)+" B:"+Float.toString(positionB));
        }
	
        square[size-1] = (Point3f) square[0].clone();
        return square;
    
    }
    
    private Point3f[] createSquareLegoMan(int size){
        Point3f[] square = new Point3f[size];
        float progressA = (float) 0.14;
        for(int a=0; a < size; a++){
            float positionA = progressA*a;
            square[a] = new Point3f(positionA,0.04f,-0.4f); 
        }
	
        square[size-1] = (Point3f) square[0].clone();
        return square;
    
    }
    
    private Point3f[] createSquareHelicopter(int size){
        Point3f[] square = new Point3f[size];
        
        float progressA = (float) 0.3;
        for(int a=1; a < size+1; a++){
            float positionA = progressA*a;
            square[a-1] = new Point3f(positionA,-0.1f,-0.1f); 
        }
	
        square[size-1] = (Point3f) square[0].clone();
        return square;
    
    }
    
    private float[] createUniformAnimKnots(int number) {
        float[] knots = new float[number];
        for (int i = 0; i < number; i++){
            knots[i] = ((float)1.0 * i) / (float) (number - 1);
        }
        return knots;
    }


}
